package org.example.logparser;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import static org.junit.Assert.assertEquals;

public class LogParserTest {

    private LogParser logParser;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    private File file;
    private BlockingQueue<LogLine> logsQueue;
    private BlockingQueue<Long> trafficQueue;
    private BufferedReader bufferedReader;
    private FileInputStream inputStream;

    @Before
    public void setUp() throws IOException {
        this.file = folder.newFile("access.log");
        this.logsQueue = new LinkedBlockingDeque<>();
        this.trafficQueue = new LinkedBlockingDeque<>();
        logParser = new LogParser(file.getAbsolutePath(), logsQueue, trafficQueue);
        inputStream = new FileInputStream(file.getAbsolutePath());
        this. bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
    }

    @After
    public void tearDown() throws IOException {
        bufferedReader.close();
        inputStream.close();
    }

    @Test
    public void testLogParserContinuousWorkSkipsInvalidLine() throws Exception {
        // given
        FileWriter fw1 = new FileWriter(file);
        BufferedWriter bw1 = new BufferedWriter( fw1 );
        bw1.write(
                "\"remotehost\",\"rfc931\",\"authuser\",\"date\",\"request\",\"status\",\"bytes\"\n" + // header => is skipped
                "\n" + // empty line => is skipped
                "\"-\",\"apache\",1549573860,\"GET /api/user HTTP/1.0\",200,1234\n" + // missing column => is skipped
                "\"\",\"-\",\"apache\",1549573860,\"GET /api/user HTTP/1.0\",200,1234\n" + // empty host => is skipped
                ",\"-\",\"apache\",1549573860,\"GET /api/user HTTP/1.0\",200,1234\n" + // empty host => is skipped
                "\"10.0.0.2\",\"-\",\"apache\",abc,\"GET /api/user HTTP/1.0\",200,1234\n" + // invalid timestamp => is skipped
                "\"10.0.0.2\",\"-\",\"apache\",1549573860,\"GET HTTP/1.0\",200,1234\n" + // invalid request => is skipped
                "\"10.0.0.2\",\"-\",\"apache\",1549573860,\"GET /api/user HTTP/1.0\",abc,1234\n" + // invalid status => is skipped
                "\"10.0.0.2\",\"-\",\"apache\",1549573860,\"GET /api/user HTTP/1.0\",200,abc\n" // invalid bytes => is skipped
        );
        bw1.close();

        // when
        for (int i=0; i<9; i++) {
            logParser.doContinuousWork(bufferedReader);
        }

        // then
        assertEquals(0, logsQueue.size());
        assertEquals(0, trafficQueue.size());
    }

    @Test
    public void testLogParserContinuousWorkMapsValidLine() throws Exception {
        // given
        FileWriter fw1 = new FileWriter(file);
        BufferedWriter bw1 = new BufferedWriter( fw1 );
        bw1.write(
                "\"remotehost\",\"rfc931\",\"authuser\",\"date\",\"request\",\"status\",\"bytes\"\n" + // header => is skipped
                "\"10.0.0.2\",\"-\",\"apache\",1549573860,\"GET /api/user HTTP/1.0\",200,1234\n" + // first valid log
                "\"10.0.0.2\",\"-\",\"apache\",1549573861,\"GET /report HTTP/1.0\",200,1234\n" // second valid log
        );
        bw1.close();

        // when
        for (int i=0; i<3; i++) {
            logParser.doContinuousWork(bufferedReader);
        }

        // then
        assertEquals(2, logsQueue.size());
        assertEquals("/api", logsQueue.peek().getWebsiteSection());
        assertEquals(Long.valueOf(1549573860), logsQueue.take().getDate());
        assertEquals("/report", logsQueue.peek().getWebsiteSection());
        assertEquals(Long.valueOf(1549573861), logsQueue.take().getDate());

        assertEquals(2, trafficQueue.size());
        assertEquals(Long.valueOf(1549573860), trafficQueue.take());
        assertEquals(Long.valueOf(1549573861), trafficQueue.take());
    }
}