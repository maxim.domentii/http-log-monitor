package org.example.displayer;

import org.example.alertingservice.AlertType;
import org.example.alertingservice.AlertingService;
import org.example.logparser.LogLine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingDeque;

import static org.junit.Assert.*;

public class DisplayerTest {

    private Displayer displayer;
    private BlockingQueue<LogLine> logsQueue;
    private ConcurrentMap<String, Object> alert;
    private ByteArrayOutputStream console;

    @Before
    public void setUp() throws Exception {
        this.logsQueue = new LinkedBlockingDeque<>();
        this.alert = new ConcurrentHashMap<>();
        this.displayer = new Displayer(10, logsQueue, alert);
        this.console = new ByteArrayOutputStream();
        System.setOut(new PrintStream(console));
    }

    @After
    public void tearDown() throws Exception {
        console.reset();
    }

    @Test
    public void testCheckAndPrintTrafficAlertWhenNoPrint() {
        // given
        alert.put(AlertingService.ALERT_SHOW_KEY, false);

        // when
        displayer.checkAndPrintTrafficAlert();

        // then
        assertEquals("", console.toString());
    }

    @Test
    public void testCheckAndPrintTrafficAlertWhenHighTraffic() {
        // given
        alert.put(AlertingService.ALERT_SHOW_KEY, true);
        alert.put(AlertingService.ALERT_TYPE_KEY, AlertType.HIGH_TRAFFIC);

        // when
        displayer.checkAndPrintTrafficAlert();

        // then
        assertTrue(console.toString().contains("WARNING: High traffic alert"));
    }

    @Test
    public void testCheckAndPrintTrafficAlertWhenTrafficBackToNormal() {
        // given
        alert.put(AlertingService.ALERT_SHOW_KEY, true);
        alert.put(AlertingService.ALERT_TYPE_KEY, AlertType.NORMAL_TRAFFIC);

        // when
        displayer.checkAndPrintTrafficAlert();

        // then
        assertTrue(console.toString().contains("INFO: Traffic is back to normal"));
    }

    @Test
    public void testGetLastStatsTimeReturnNullWhenNotSetAndQueueEmpty() throws InterruptedException {
        // given
        Long lastStatsTime = null;
        logsQueue.clear();

        // when
        Long result = displayer.getLastStatsTime(lastStatsTime);

        // then
        assertNull(result);
    }

    @Test
    public void testGetLastStatsTimeReturnSameWhenSetAndQueueEmpty() throws InterruptedException {
        // given
        Long lastStatsTime = 123456789L;
        logsQueue.clear();

        // when
        Long result = displayer.getLastStatsTime(lastStatsTime);

        // then
        assertEquals(lastStatsTime, result);
    }

    @Test
    public void testGetLastStatsTimeSetInitialLastStatsTimeWhenQueueNotEmpty() throws InterruptedException {
        // given
        Long lastStatsTime = null;
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(1L).build());
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(2L).build());

        // when
        Long result = displayer.getLastStatsTime(lastStatsTime);

        // then
        assertEquals(Long.valueOf(1), result);
    }

    @Test
    public void testGetLastStatsTimeUpdateLastStatsTimePrintStats() throws InterruptedException {
        // given
        Long lastStatsTime = 1L;
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(11L).withUuid(UUID.randomUUID()).build());
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(12L).withUuid(UUID.randomUUID()).build());
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(13L).withUuid(UUID.randomUUID()).build());

        // when
        Long result = displayer.getLastStatsTime(lastStatsTime);

        // then
        assertEquals(Long.valueOf(13), result);
        assertEquals(0, logsQueue.size());
    }

    @Test
    public void testGetLastStatsTimeNoPrintForSmallerIntervalThanStatsInterval() throws InterruptedException {
        // given
        Long lastStatsTime = 1L;
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(2L).withUuid(UUID.randomUUID()).build());
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(9L).withUuid(UUID.randomUUID()).build());
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(10L).withUuid(UUID.randomUUID()).build());

        // when
        Long result = displayer.getLastStatsTime(lastStatsTime);

        // then
        assertEquals(Long.valueOf(1), result);
        assertEquals(3, logsQueue.size());
        assertEquals("", console.toString());
    }

    @Test
    public void testGetLastStatsTimePrintStats() throws InterruptedException {
        // given
        Long lastStatsTime = 1L;
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(9L).withUuid(UUID.randomUUID())
                .withWebsiteSection("/api").build());
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(10L).withUuid(UUID.randomUUID())
                .withWebsiteSection("/api").build());
        logsQueue.put(LogLine.LogLineBuilder.aLogLine().withDate(11L).withUuid(UUID.randomUUID())
                .withWebsiteSection("/report").build());

        // when
        Long result = displayer.getLastStatsTime(lastStatsTime);

        // then
        assertEquals(Long.valueOf(11), result);
        assertEquals(0, logsQueue.size());
        assertTrue(console.toString().contains("Stats during the last 10 seconds at 11"));
        assertTrue(console.toString().contains("Website section /api: 2 hits."));
        assertTrue(console.toString().contains("Percentage of total traffic for this interval: " + 2.0/3 * 100));
        assertTrue(console.toString().contains("Website section /report: 1 hits."));
        assertTrue(console.toString().contains("Percentage of total traffic for this interval: " + 1.0/3 * 100));
        assertTrue(console.toString().contains("Total hits for this interval: 3"));
    }
}