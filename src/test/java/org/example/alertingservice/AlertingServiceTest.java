package org.example.alertingservice;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingDeque;

import static org.junit.Assert.assertEquals;

public class AlertingServiceTest {

    private AlertingService alertingService;
    private BlockingQueue<Long> trafficQueue;
    private ConcurrentMap<String, Object> alert;

    @Before
    public void setUp() throws Exception {
        this.trafficQueue = new LinkedBlockingDeque<>();
        this.alert = new ConcurrentHashMap<>();
        this.alertingService = new AlertingService(2, 1, trafficQueue, alert);
        this.alert.put(AlertingService.ALERT_SHOW_KEY, false);
        this.alert.put(AlertingService.ALERT_TYPE_KEY, AlertType.NORMAL_TRAFFIC);
    }

    @Test
    public void testDoContinuousWorkNoAlertWhenNormalTraffic() throws InterruptedException {
        // given
        trafficQueue.put(1L);
        trafficQueue.put(10L);
        trafficQueue.put(12L);
        trafficQueue.put(20L);

        // when
        for (int i=0; i<=4; i++) {
            alertingService.doContinuousWork();
        }

        // then
        assertEquals(false, alert.get(AlertingService.ALERT_SHOW_KEY));
    }

    @Test
    public void testDoContinuousWorkHighTrafficAlert() throws InterruptedException {
        // given
        trafficQueue.put(1L);
        trafficQueue.put(1L);
        trafficQueue.put(1L);

        // when
        for (int i=0; i<=3; i++) {
            alertingService.doContinuousWork();
        }

        // then
        assertEquals(true, alert.get(AlertingService.ALERT_SHOW_KEY));
        assertEquals(AlertType.HIGH_TRAFFIC, alert.get(AlertingService.ALERT_TYPE_KEY));
        assertEquals(3, alert.get(AlertingService.ALERT_TOTAL_TRAFFIC_KEY));
    }
}