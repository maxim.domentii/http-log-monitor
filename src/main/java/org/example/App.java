package org.example;

import org.example.alertingservice.AlertType;
import org.example.alertingservice.AlertingService;
import org.example.config.AppConfigs;
import org.example.displayer.Displayer;
import org.example.logparser.LogLine;
import org.example.logparser.LogParser;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingDeque;

public class App {
    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Shutdown");
        }));

        AppConfigs appConfigs = new AppConfigs();
        BlockingQueue<LogLine> logsQueue = new LinkedBlockingDeque<>();
        BlockingQueue<Long> trafficQueue = new LinkedBlockingDeque<>();
        ConcurrentMap<String, Object> alert = new ConcurrentHashMap<>();
        alert.put(AlertingService.ALERT_SHOW_KEY, false);
        alert.put(AlertingService.ALERT_TYPE_KEY, AlertType.NORMAL_TRAFFIC);

        LogParser logParser = new LogParser(
                appConfigs.getLogParserConfigs().getLogFilePath(), logsQueue, trafficQueue);
        Displayer displayer = new Displayer(appConfigs.getDisplayerConfigs().getStatsInterval(), logsQueue, alert);
        AlertingService alertingService = new AlertingService(appConfigs.getAlertingServiceConfigs().getThreshold(),
                appConfigs.getAlertingServiceConfigs().getAlertInterval(), trafficQueue, alert);

        new Thread(logParser).start();
        new Thread(displayer).start();
        new Thread(alertingService).start();
    }
}
