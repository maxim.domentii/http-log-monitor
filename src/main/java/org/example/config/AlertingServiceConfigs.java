package org.example.config;

import java.util.Properties;

public class AlertingServiceConfigs extends Configs {

    private int threshold;
    private int alertInterval;

    public AlertingServiceConfigs(Properties properties) {
        super(properties);
    }

    public int getThreshold() {
        return threshold;
    }

    public int getAlertInterval() {
        return alertInterval;
    }

    @Override
    protected void initConfigs(Properties properties) {
        int threshold = Integer.parseInt(properties.getProperty("alertingService.threshold", "10"));
        if (threshold < 1) {
            throw new RuntimeException("Invalid alertingService.threshold property. Has to be < 1");
        }
        this.threshold = threshold;

        int alertInterval = Integer.parseInt(properties.getProperty("alertingService.alertInterval", "120"));
        if (alertInterval < 1) {
            throw new RuntimeException("Invalid alertingService.alertInterval property. Has to be < 1");
        }
        this.alertInterval = alertInterval;
    }
}
