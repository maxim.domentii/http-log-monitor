package org.example.config;

import java.util.Properties;

public class LogParserConfigs extends Configs {

    private String logFilePath;

    public LogParserConfigs(Properties properties) {
        super(properties);
    }

    public String getLogFilePath() {
        return logFilePath;
    }

    @Override
    protected void initConfigs(Properties properties) {
        this.logFilePath = properties.getProperty("parser.logFilePath", "/tmp/access.log");
    }
}
