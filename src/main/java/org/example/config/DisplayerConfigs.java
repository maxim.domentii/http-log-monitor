package org.example.config;

import java.util.Properties;

public class DisplayerConfigs extends Configs {

    private int statsInterval;

    public DisplayerConfigs(Properties properties) {
        super(properties);
    }

    public int getStatsInterval() {
        return statsInterval;
    }

    @Override
    protected void initConfigs(Properties properties) {
        int statsInterval = Integer.parseInt(properties.getProperty("displayer.statsInterval", "10"));
        if (statsInterval < 1) {
            throw new RuntimeException("Invalid displayer.statsInterval property. Has to be < 1");
        }
        this.statsInterval = statsInterval;
    }
}
