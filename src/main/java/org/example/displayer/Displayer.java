package org.example.displayer;

import org.example.alertingservice.AlertType;
import org.example.alertingservice.AlertingService;
import org.example.logparser.LogLine;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

/**
 * This thread displays the stats for every statsInterval (default 10 s) based on logs timestamp
 * and the traffic alert when it is higher than threshold for alertInterval.
 */
public class Displayer implements Runnable {

    private int statsInterval;
    private BlockingQueue<LogLine> logsQueue;
    private ConcurrentMap<String, Object> alert;

    public Displayer(int statsInterval, BlockingQueue<LogLine> logsQueue, ConcurrentMap<String, Object> alert) {
        this.statsInterval = statsInterval;
        this.logsQueue = logsQueue;
        this.alert = alert;
    }

    /**
     * First time it will set the first log timestamp from logsQueue as lastStatsTime.
     * Then it checks the diff between last parsed log timestamp from the logsQueue and lastStatsTime.
     * If the diff is greater than statInterval:
     * <ul>
     *  <li/> it takes all the logs from the logsQueue
     *  <li/> builds a map with the count of each website section
     *  <li/> sort descending the sections based on count
     *  <li/> print the statistics
     * </ul>
     * Also, if {@link AlertingService} set the show property to true in the alert map object then it prints the
     * relevant alter (high traffic or back to normal traffic)     *
     */
    @Override
    public void run() {
        try {
            printHeader();

            Long lastStatsTime = null;
            while(true) {
                // Alert displaying
                checkAndPrintTrafficAlert();

                // Stats displaying
                lastStatsTime = getLastStatsTime(lastStatsTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    protected Long getLastStatsTime(Long lastStatsTime) throws InterruptedException {
        if (lastStatsTime == null){
            lastStatsTime = getFirstLogTime();
            return lastStatsTime;
        }

        LogLine lastParsedLog = getLastParsedLog();
        if (lastParsedLog == null) {
            return lastStatsTime;
        }
        Long currentLogTime = lastParsedLog.getDate();

        long timeDiff = currentLogTime - lastStatsTime;
        if (timeDiff >= statsInterval) {
            lastStatsTime = currentLogTime;
            printStats(lastParsedLog);
        }
        return lastStatsTime;
    }

    protected void checkAndPrintTrafficAlert() {
        if (alert.containsKey(AlertingService.ALERT_SHOW_KEY)
                && (boolean)alert.get(AlertingService.ALERT_SHOW_KEY)){
            if (AlertType.HIGH_TRAFFIC.equals(alert.get(AlertingService.ALERT_TYPE_KEY))){
                printHighTrafficAlert();
            } else {
                printHighTrafficRecoveryAlert();
            }
            alert.put(AlertingService.ALERT_SHOW_KEY, false);
        }
    }

    private void printHighTrafficAlert() {
        StringBuilder sb = new StringBuilder();
        sb.append("**********************************************************\n");
        sb.append("WARNING: High traffic alert triggered at ").append(alert.get(AlertingService.ALERT_TIMESTAMP_KEY));
        sb.append("\nThere are ").append(alert.get(AlertingService.ALERT_TOTAL_TRAFFIC_KEY));
        sb.append(" hits in the last ").append(alert.get(AlertingService.ALERT_INTERVAL_KEY)).append(" seconds.\n");
        sb.append("**********************************************************\n");
        System.out.println(sb);
    }

    private void printHighTrafficRecoveryAlert() {
        StringBuilder sb = new StringBuilder();
        sb.append("**********************************************************\n");
        sb.append("INFO: Traffic is back to normal at ").append(alert.get(AlertingService.ALERT_TIMESTAMP_KEY));
        sb.append("\n**********************************************************\n");
        System.out.println(sb);
    }

    private void printHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append("########################\n");
        sb.append("### HTTP Log Monitor ###\n");
        sb.append("########################\n");
        System.out.println(sb);
    }

    private void printStats(LogLine lastParsedLog) throws InterruptedException {
        long currentLogTime = lastParsedLog.getDate();
        UUID uuidOfLastParsedLog = lastParsedLog.getUuid();
        Map<String, Integer> sectionCountMap = new HashMap<>();
        while (!logsQueue.isEmpty()) {
            LogLine logLine = logsQueue.take();
            String websiteSection = logLine.getWebsiteSection();
            sectionCountMap.put(websiteSection, sectionCountMap.getOrDefault(websiteSection, 0) + 1);
            if (uuidOfLastParsedLog.equals(logLine.getUuid())){
                break;
            }
        }

        Set<Map.Entry<String, Integer>> sortedByCountSections = sectionCountMap.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .collect(Collectors.toCollection(LinkedHashSet::new));

        int totalTraffic = sectionCountMap.values().stream().reduce(0, Integer::sum);

        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("Stats during the last ").append(statsInterval).append(" seconds at ").append(currentLogTime).append("\n");
        sb.append("---------------------------------------------------------------------\n");
        for  (Map.Entry<String, Integer> sectionCount : sortedByCountSections) {
            String section = sectionCount.getKey();
            Integer count = sectionCount.getValue();
            sb.append("Website section ").append(section).append(": ").append(count).append(" hits. ");
            sb.append("Percentage of total traffic for this interval: ").append(count*1.0 / totalTraffic * 100).append(" %\n");
        }
        sb.append("Total hits for this interval: ").append(totalTraffic).append("\n");
        System.out.println(sb);
    }

    private LogLine getLastParsedLog() {
        if (logsQueue.isEmpty()) {
            return null;
        }
        LogLine[] logs = logsQueue.toArray(new LogLine[0]);
        return logs[logs.length-1];
    }

    private Long getFirstLogTime() {
        if (logsQueue.isEmpty()) {
            return null;
        }
        return logsQueue.peek().getDate();
    }
}
