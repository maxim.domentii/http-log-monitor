package org.example.logparser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

/**
 * This thread reads the logs file line by line, parse the line and add it into a queue shared with
 * {@link org.example.displayer.Displayer}. It also adds the timestamp of each log line in a second queue shared with
 * {@link org.example.alertingservice.AlertingService} to monitor the traffic for the last alertInterval time
 * (default is 2 min)
 */
public class LogParser implements Runnable {

    private String logFilePath;
    private BlockingQueue<LogLine> logsQueue;
    private BlockingQueue<Long> trafficQueue;

    public LogParser(String logFilePath, BlockingQueue<LogLine> logsQueue, BlockingQueue<Long> trafficQueue) {
        this.logFilePath = logFilePath;
        this.logsQueue = logsQueue;
        this.trafficQueue = trafficQueue;
    }

    /**
     * First it opens a input stream to the log file and a bufferReader with that input stream.
     * Until the thread is killed:
     * <ul>
     *     <li/> Reads next available line in the file
     *     <li/> Parse the line and skip it if is has a wrong format
     *     <li/> Add the parsed line in the logsQueue and timestamp of it in the trafficQueue
     * </ul>
     */
    @Override
    public void run() {
        try {
            FileInputStream inputStream = null;
            BufferedReader bufferedReader = null;
            try {
                inputStream = new FileInputStream(logFilePath);
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                while (true) {
                    doContinuousWork(bufferedReader);
                }
            } finally {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    protected void doContinuousWork(BufferedReader bufferedReader) throws Exception {
        String line = bufferedReader.readLine();
        if (line != null) {
            LogLine logLine = parseLogLine(line);
            if (logLine != null) {
                logsQueue.put(logLine);
                trafficQueue.put(logLine.getDate());
            }
        }
    }

    private LogLine parseLogLine(String line) {
        String[] tokens = line.split(",");
        if (tokens.length < 7) {
            return null;
        }

        String hostname = tokens[0].length() > 2 ? tokens[0].substring(1, tokens[0].length()-1) : "";
        if (hostname.isEmpty()) {
            return null;
        }
        String rfc931 = tokens[1].length() > 2 ? tokens[1].substring(1, tokens[1].length()-1) : "";
        if (rfc931.isEmpty()) {
            return null;
        }
        String user = tokens[2].length() > 2 ? tokens[2].substring(1, tokens[2].length()-1) : "";
        if (user.isEmpty()) {
            return null;
        }
        String websiteSection = parseWebsiteSection(tokens[4]);
        if (websiteSection.isEmpty()) {
            return null;
        }

        long date;
        int status;
        int bytes;
        try {
            date = Long.parseLong(tokens[3]);
            status = Integer.parseInt(tokens[5]);
            bytes = Integer.parseInt(tokens[6]);
        } catch (NumberFormatException e) {
            return null;
        }
        if (date < 0) {
            return null;
        }
        if (status < 0) {
            return null;
        }
        if (bytes < 0) {
            return null;
        }

        return LogLine.LogLineBuilder.aLogLine()
                .withUuid(UUID.randomUUID())
                .withHostname(hostname)
                .withRfc931(rfc931)
                .withUser(user)
                .withDate(date)
                .withWebsiteSection(websiteSection)
                .withStatus(status)
                .withBytes(bytes)
                .build();
    }

    private String parseWebsiteSection(String token) {
        int startIndex = token.indexOf("/");
        if (startIndex < 0) {
            return "";
        }

        int secondSlashIndex = token.indexOf("/", startIndex + 1);
        int spaceIndex = token.indexOf(" ", startIndex + 1);
        int endIndex = Math.min(spaceIndex, secondSlashIndex);
        if (endIndex < 0) {
            return "";
        }

        return token.substring(startIndex, endIndex);
    }
}
