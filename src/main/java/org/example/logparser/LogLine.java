package org.example.logparser;

import java.util.UUID;

/**
 * This is a POJO class representing a log line from the log file.
 */
public class LogLine {
    private UUID uuid;
    private String hostname;
    private String rfc931;
    private String user;
    private Long date;
    private String websiteSection;
    private int status;
    private int bytes;

    public UUID getUuid() {
        return uuid;
    }

    public String getHostname() {
        return hostname;
    }

    public String getRfc931() {
        return rfc931;
    }

    public String getUser() {
        return user;
    }

    public Long getDate() {
        return date;
    }

    public String getWebsiteSection() {
        return websiteSection;
    }

    public int getStatus() {
        return status;
    }

    public int getBytes() {
        return bytes;
    }

    public static final class LogLineBuilder {
        private UUID uuid;
        private String hostname;
        private String rfc931;
        private String user;
        private Long date;
        private String websiteSection;
        private int status;
        private int bytes;

        private LogLineBuilder() {
        }

        public static LogLineBuilder aLogLine() {
            return new LogLineBuilder();
        }

        public LogLineBuilder withUuid(UUID uuid) {
            this.uuid = uuid;
            return this;
        }

        public LogLineBuilder withHostname(String hostname) {
            this.hostname = hostname;
            return this;
        }

        public LogLineBuilder withRfc931(String rfc931) {
            this.rfc931 = rfc931;
            return this;
        }

        public LogLineBuilder withUser(String user) {
            this.user = user;
            return this;
        }

        public LogLineBuilder withDate(Long date) {
            this.date = date;
            return this;
        }

        public LogLineBuilder withWebsiteSection(String websiteSection) {
            this.websiteSection = websiteSection;
            return this;
        }

        public LogLineBuilder withStatus(int status) {
            this.status = status;
            return this;
        }

        public LogLineBuilder withBytes(int bytes) {
            this.bytes = bytes;
            return this;
        }

        public LogLine build() {
            LogLine logLine = new LogLine();
            logLine.uuid = this.uuid;
            logLine.bytes = this.bytes;
            logLine.hostname = this.hostname;
            logLine.status = this.status;
            logLine.date = this.date;
            logLine.websiteSection = this.websiteSection;
            logLine.user = this.user;
            logLine.rfc931 = this.rfc931;
            return logLine;
        }
    }
}
