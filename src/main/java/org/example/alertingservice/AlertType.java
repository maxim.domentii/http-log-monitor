package org.example.alertingservice;

/**
 * Enum for alert type to be printed for high traffic or back to normal traffic.
 */
public enum AlertType {
    HIGH_TRAFFIC, NORMAL_TRAFFIC
}
