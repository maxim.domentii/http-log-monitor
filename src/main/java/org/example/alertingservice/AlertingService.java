package org.example.alertingservice;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;

/**
 * This thread calculates if {@link org.example.displayer.Displayer} needs to print a high traffic or back to normal
 * traffic alert. For this purpose it calculate if the total traffic for the past alertInterval (default 2 min)
 * is higher than the threshold (default 10 req/sec).
 */
public class AlertingService implements Runnable {

    public static final String ALERT_TYPE_KEY = "type";
    public static final String ALERT_TOTAL_TRAFFIC_KEY = "totalTraffic";
    public static final String ALERT_SHOW_KEY = "show";
    public static final String ALERT_TIMESTAMP_KEY = "timestamp";
    public static final String ALERT_INTERVAL_KEY = "alertInterval";

    private int threshold;
    private int alertInterval;
    private BlockingQueue<Long> trafficQueue;
    private ConcurrentMap<String, Object> alert;
    private LinkedList<Long> trafficTimestamps;

    public AlertingService(int threshold, int alertInterval, BlockingQueue<Long> trafficQueue,
                           ConcurrentMap<String, Object> alert) {
        this.threshold = threshold;
        this.alertInterval = alertInterval;
        this.trafficQueue = trafficQueue;
        this.alert = alert;
        this.trafficTimestamps = new LinkedList<>();
    }

    /**
     * It keeps a sliding window array of the timestamps in the trafficQueue which are within the alertInterval.
     * For every new timestamp taken from the trafficQueue:
     * <ul>
     *     <li/> update the sliding window array
     *     <li/> check if traffic is higher than the threshold or is back to normal
     *     <li/> set the alert map object shared with {@link org.example.displayer.Displayer}
     * </ul>
     */
    @Override
    public void run() {
        try {
            while(true) {
                doContinuousWork();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    protected void doContinuousWork() {
        updateTrafficTimestampsForAlertingInterval();

        if (isTrafficHigherThanThreshold() && !AlertType.HIGH_TRAFFIC.equals(alert.get(ALERT_TYPE_KEY))) {
            setHighTrafficAlertDetails();
        }
        if (isTrafficWithinThreshold() && !AlertType.NORMAL_TRAFFIC.equals(alert.get(ALERT_TYPE_KEY))) {
            setHighTrafficRecoverAlertDetails();
        }

        if (!trafficQueue.isEmpty()) {
            trafficTimestamps.add(trafficQueue.poll());
        }
    }

    private void setHighTrafficRecoverAlertDetails() {
        alert.put(ALERT_TYPE_KEY, AlertType.NORMAL_TRAFFIC);
        setCommonTrafficAlertDetails();
    }

    private void setHighTrafficAlertDetails() {
        alert.put(ALERT_TOTAL_TRAFFIC_KEY, trafficTimestamps.size());
        alert.put(ALERT_TYPE_KEY, AlertType.HIGH_TRAFFIC);
        setCommonTrafficAlertDetails();
    }

    private void setCommonTrafficAlertDetails() {
        alert.put(ALERT_SHOW_KEY, true);
        alert.put(ALERT_TIMESTAMP_KEY, !trafficTimestamps.isEmpty() ? trafficTimestamps.getLast(): "");
        alert.put(ALERT_INTERVAL_KEY, alertInterval);
    }

    private void updateTrafficTimestampsForAlertingInterval() {
        if (!trafficTimestamps.isEmpty()) {
            long lastTimestamp = trafficTimestamps.getLast();
            Iterator<Long> trafficTimestampsIterator = trafficTimestamps.iterator();
            while (trafficTimestampsIterator.hasNext()) {
                Long timestamp = trafficTimestampsIterator.next();
                if (lastTimestamp - timestamp > alertInterval) {
                    trafficTimestampsIterator.remove();
                } else {
                    break;
                }
            }
        }
    }

    private boolean isTrafficHigherThanThreshold() {
        return (1.0 * trafficTimestamps.size() / alertInterval) > 1.0 * threshold;
    }

    private boolean isTrafficWithinThreshold() {
        return (1.0 * trafficTimestamps.size() / alertInterval) <= 1.0 * threshold;
    }
}
