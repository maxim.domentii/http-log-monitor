# HTTP Log Monitoring Console Program

## Requirements

* Read a CSV-encoded HTTP access log. It should either take the file as a
  parameter or read from standard input. All time-based calculations should be
  relative to the timestamps in the log-file. Avoid using system time in your
  program. 

Example log file (first line is the header):
```csv
"remotehost","rfc931", "authuser","date", "request","status", "bytes"
"18.0.0.1","-", "apache", 1549574332, "GET /api/user HTTP/1.0", 280, 1234
*10.0.0.4","-", "apache", 1549574333, "GET /report HTTP/1.0",200, 1136
"18.0.0.1", "-", "apache", 1549574334, "GET /api/user HTTP/1.0", 200, 1194
*10.0.0.4","-","apache", 1549574334, "POST /report HTTP/1.0",484,1397
```
* For every 10 seconds of log lines, display stats about the traffic during those 10s:
  the sections of the web site with the most hits, as well as statistics that might be
  useful for debugging. A section is defined as being what's before the second in
  the resource section of the log line. For example, the section for "/api/user" is
  "/api" and the section for "/report" is "/report"
* Whenever total traffic for the past 2 minutes exceeds a certain number on
  average, print a message to the console saying that "High traffic generated an
  alert- hits = {value}, triggered at {time)". The default threshold should be 10
  requests per second but should be configurable
* Whenever the total traffic drops again below that value on average for the past 2
  minutes, print another message detailing when the alert recovered, +/- a second
* Consider the efficiency of your solution and how it would scale to process high
  volumes of log lines - don't assume you can read the entire file into memory
* Write your solution as you would any piece of code that others might need to
  modify and maintain, both in terms of structure and style
* Write a test for the alerting logic
* Explain how you'd improve on this application design

## Implementation

The project is designed as a Pub-Sub pattern with one producer component - LogParser and two consumers components - 
Displayer and AlertingService:

* The LogParser component reads each line of the log file, parses the lines and map it to a POJO class.
Puts the parsed data in a queue shared with a Displayer component in order to print the stats. 
The LogParser also shares a queue with the AlertingService component, containing the timestamp of each line of the log file.
In this way the file is not loaded in memory. Only the logs for statistics interval (10s) and timestamps for alerting
interval (2min) are kept in memory.

* The Displayer component displays the statistics every 10 seconds, by reading the queue shared with the LogParser
and prints the alert raised by AlertingService by getting the information from a map shared by these two components.

* The AlertingService checks if an alert message should be raised by getting the elements from the queue shared with the
LogParser component, and keeping a sliding window array with the elements that are at most 2 min old.

Each of these components is running in a different thread to enable reading the same log file, displaying the stats 
and the alert messages, and checking if an alert message should be raised. 
The advantage of this structure is that each element is independent of the others. 
If we want to change our parser, it will be easy. Moreover, we can add many parsers to put the lines in the same queue. 
We can apply the same logic to the displayer and the alerting service.

Configuration properties are set in the application.properties file which is loaded and validated at startup.

There is also a main class App which loads the properties, configure the thread and start them.

## Limitations and Improvements

* One limitation of this implementation is given by the queues used to keep logs for statistics and alert interval.
For a big throughput of logs, where there are enough logs in the calculated interval may lead to memory issues.
As a possible solution we could use a persistence layer like a message broker or a database.

* The console ouput is not nice at all. We could invest some more time in that to make it more user-friendly.

* At the moment the output is printed in the console. We could change that to an output stream which can be integrated
afterward with any implementation, so we could keep a history of statistics and alerts, trigger email alert, integrate 
with a UI implementation etc.

* We could set up an integration test suite and a CI/CD pipeline for further improvement iterations to automatize the
processes.

## Prerequisites
* Java 1.8 or later
* Maven 

## Build
```shell
mvn clean package
```

## Run
```shell
java -jar target/http-log-monitor-1.0-SNAPSHOT.jar
```

## Run unit tests
```shell
mvn verify
```